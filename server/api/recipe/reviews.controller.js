/***
 * Group project done by
 * Deodatta Baral
 * Suraaj Shrestha
 */
'use strict';
import { Review, Recipe, User } from './recipe.model';


export function index(req, res) {

    Review.find({ recipe: req.params.recipeId })

      /*
      exec() runs the query and returns a promise object.
      For each user object, populate the address attribute.
      This then method will only be called if the query was successful, so no need to error check!
       */
        .populate('user')
        .exec()
        .then(function (rev) {
            res.json({
                rev
            });
        })
        /*
       Any errors encountered here must be server side, since there are no arguments to the find
       Return 500 (server error) and send the error encountered back to the requester
      */
        .catch(function (err) {
            res.status(500);
            res.send(err);
        });
}

//This will use the id to find the reviews
export function show(req, res) {
          /*
         findById will return null if the object was not found
         This if check will evaluate to false for a null user
         and return the status based on the result of the if statement.
          */
    Review.findById(req.params.id)
        .populate('user')
        .exec()
        .then(function (is) {
            if (is) {
                res.status(200);
                res.json(is);
            } else {
                res.status(404);
                res.json({ message: 'Not Found' });
            }
        })
        .catch(function (err) {
            res.status(400);
            res.send(err);
        });
}

/*
This is creating a review with the info that is provided by the user
Find the username body parameter to obtain Object id.
After retrieving the body and id, a new review will be create
as the review and recipe are interlinked, it is essential to update the review side on the recipe id
functions are nested accordingly
*/

export function create(req, res) {
/*
 In this function we are taking the request body
 As it was sent and using it as the JSON for the address
 and review objects.
 **/
 let rev = req.body;
  /*
  That means that the following .then in this chain
   will not occur until after the user is saved, and will be given the result
   of this promise resolving, which is the created user object
  */

 User.findOne({ username: rev.username }).exec()
   .then(function (user) {

         var RevNew = {
             recipe: req.params.recipeId,
             description: rev.description,
             rating: rev.rating,
             user: user._id
         };

         Review.create(RevNew)
           .then(function (cRev) {
             Recipe.findById(req.params.recipeId)
               .populate('review')
               .exec()
               .then(function (recipe) {
                 if (recipe) {
                   recipe.review.push(cRev);
                   recipe.save(function (err) {
                     if (err) {
                       throw err;
                     }
                     res.status(201);
                     res.json(cRev);
                   });
                     }
                     })
               .catch(function (err) {
                 res.status(400);
                 res.send(err);
               });
             })
             .catch(function (err) {
               res.status(400);
               res.send(err);
             });

     }).catch(function (err) {
       res.status(400);
       res.send(err);
     });
}


export function update(req, res) {
 Review.findById(req.params.id)
     .populate('user')
     .exec()
     .then(function (isRev) {
         if (isRev) {
           isRev.rating = req.body.rating;
           isRev.description = req.body.description;
           isRev.date = Date.now();
             return Promise.all([
               isRev.increment().save()
             ]);
         } else {
             return isRev;
         }
     })

     .then(function (Objects) {
         if (Objects) {
             res.status(200);
             res.json(Objects[0]);
         } else {
             res.status(404);
             res.json({ message: 'Not Found' });

         }

     })
     .catch(function (err) {
         res.status(400);
         res.send(err);
     });
}



export function destroy(req, res) {
 Review.findById(req.params.id)
     .populate('user')
     .exec()
     /*
       If the user was found, remove both the user object and the address object from
       their respective collections. Only record the delete as successful if both objects
       are deleted
      */
     .then(function (isRev) {
         if (isRev) {
             return Promise.all([
               isRev.remove()
             ]);
         } else {
             return isRev;
         }
     })
     // Delete was successful then send 204 success status however not found for if the element to be deleted is
     .then(function (revRemove) {
         if (revRemove) {
             Recipe.findById(req.params.recipeId)
                 .populate('review')
                 .exec()
                 .then(function (recipe) {
                     if (recipe) {
                         recipe.review.pull(revRemove[0]._id);
                         recipe.save();
                     }
                 })

                 .catch(function (err) {
                     res.status(400);
                     res.send(err);
                 });

             res.status(204).send();
         } else {
             res.status(404);
             res.json({ message: 'Not Found' });
         }
     })
     //if delete failed, then send error message
     .catch(function (err) {
         res.status(400);
         res.send(err);
     });
}
