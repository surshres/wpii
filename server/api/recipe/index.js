/***
* Group project done by
* Deodatta Baral
* Suraaj Shrestha
*/
import express from 'express';
import * as recipeC from './recipe.controller';
import * as reviewsC from './reviews.controller';
import * as userC from './users.controller';

let RouterOfRecipe = express.Router();
// GET methods
RouterOfRecipe.get('/', recipeC.index);
RouterOfRecipe.get('/:id', recipeC.show);
// POST method
RouterOfRecipe.post('/', recipeC.create);
// PUT method
RouterOfRecipe.put('/:id', recipeC.update);
// DELETE method
RouterOfRecipe.delete('/:id', recipeC.destroy);

let RouterOfReviews = express.Router({ mergeParams: true });
RouterOfRecipe.use('/:recipeId/review', RouterOfReviews);
// GET methods
RouterOfReviews.get('/', reviewsC.index);
RouterOfReviews.get('/:id', reviewsC.show);
// POST method
RouterOfReviews.post('/', reviewsC.create);
// PUT method
RouterOfReviews.put('/:id', reviewsC.update);
// DELETE method
RouterOfReviews.delete('/:id', reviewsC.destroy);

let RouterOfUser = express.Router();
// GET methods
RouterOfUser.get('/', userC.index);
RouterOfUser.get('/:id', userC.show);
// POST method
RouterOfUser.post('/', userC.create);
// PUT method
RouterOfUser.put('/:id', userC.update);
// DELETE method
RouterOfUser.delete('/:id', userC.destroy);

export { RouterOfRecipe, RouterOfReviews, RouterOfUser };
