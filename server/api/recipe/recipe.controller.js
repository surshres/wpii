/***
 * Group project done by
 * Deodatta Baral
 * Suraaj Shrestha
 */
'use strict';

import { Recipe } from './recipe.model';


export function index(req, res) {
    Recipe.find()
     /*
exec() runs the query and returns a promise object.
Promises are a cleaner way to chain asynchronous actions together than
callbacks because, instead of nesting functions within functions, you can
chain function calls together and pass the return value from one function
as the argument to the next! It also allows you to have one method to handle
exceptions, instead of having to provide them in each callback function you write
     */
        .exec()
        // This then method will only be called if the query was successful, so no need to error check!
        .then(function (rec) {
            res.json({ rec
            });
        })
        /*
       Any errors encountered here must be server side, since there are no arguments to the find
       Return 500 (server error) and send the error encountered back to the requester
      */
        .catch(function (err) {
            res.status(500);
            res.send(err);
        });
}

//This will use the id to find the recipe
export function show(req, res) {
    Recipe.findById(req.params.id)
        .exec()
        .then(function (is) {
          /*
          findById will return null if the object was not found
          This if check will evaluate to false for a null user
          and return the status based on the result of the if statement.
           */
            if (is) {
              // User was found by Id
                res.status(200);
                res.json(is);
            } else {
              // User was not found
                res.status(404);
                res.json({ message: 'Not Found' });
            }
        })
        .catch(function (err) {
            res.status(400);
            res.send(err);
        });
}

//This is creating a recipe with the info that is provided by the user

export function create(req, res) {
  /*
    In this function we are taking the request body
    As it was sent and using it as the JSON for the address
    and user objects.
    Since address is stored in a separate collection from user
    we must create each document individually, and then associate
    the address to the user after we know its id
  */
    let rec = req.body;
  /*
  That means that the following .then in this chain
   will not occur until after the user is saved, and will be given the result
   of this promise resolving, which is the created user object
  */
    Recipe.create(rec)

        // saved successfully! return 201 with the created user object
        .then(function (cRec) {
            res.status(201);
            res.json(cRec);
        })
        // An error was encountered during either the save of the address or the save of the user
        .catch(function (err) {
            res.status(400);
            res.send(err);
        });
}


// Update a user
export function update(req, res) {
  // Start by trying to find the user by its id
    Recipe.findById(req.params.id)
        .exec()
        // Update user and address
        .then(function (isRec) {
          // If user exists, update all fields of the object
           if (isRec) {
             isRec.name = req.body.name;
             isRec.description = req.body.description;
             isRec.pictureURL = req.body.pictureURL;
             isRec.prepTime = req.body.prepTime;
             isRec.cookingTime = req.body.cookingTime;
             isRec.directions = req.body.directions;
             isRec.ingredients = req.body.ingredients;
             /*
        Promise.all takes an array of promises as an argument
        It ensures that all the promises in the array have successfully resolved before
        continuing the promise chain. It will pass to the next .then an array of results, one
        for each promise that was passed
       */
                return Promise.all([
                  isRec.increment().save()
                ]);
            } else {
             // User not found
                return isRec;
            }
        })
        // .then will be called after the Promise.all resolves, or be called with null if the user was not found
        .then(function (Objects) {
          // savedObjects should be defined if Promise.all was invoked (user was found)
          // The order of responses are guaranteed to be the same as the order of the promises, so we can assume
          // the second element of the array is the result of the user update
          if (Objects) {
                res.status(200);
                res.json(Objects[1]);
            } else {
                res.status(404);
                res.json({ message: 'Not Found' });
            }
        })

        // Error encountered during the updating of recipes
        .catch(function (err) {
            res.status(400);
            res.send(err);
        });
}


export function destroy(req, res) {
    Recipe.findById(req.params.id)
        .populate('review')
        .exec()
        .then(function (isRec) {
          /*
       If the user was found, remove both the user object and the address object from
       their respective collections. Only record the delete as successful if both objects
       are deleted
      */
            if (isRec) {
                return Promise.all([
                  isRec.remove()
                ]);
            } else {
                return isUser;
            }
        })
        // Delete was successful then send 204 success status however not found for if the element to be deleted is not found
        .then(function (removeRec) {
            if (removeRec) {
                res.status(204).send();
            } else {
                res.status(404);
                res.json({ message: 'Not Found' });
            }
        })
        //if delete failed.
        .catch(function (err) {
            res.status(400);
            res.send(err);
        });

}
