/***
* Group project done by
* Deodatta Baral
* Suraaj Shrestha
*/
import mongoose from 'mongoose';

// This is the main user schema
let S = mongoose.Schema;

/*
  This section declares the schemas for the different documents
  that will be used
 */

// Created a sub document schema as it will be used inside recipe schema
let SubIngredient = S({
   name: { type: String, required: true },
   calories: { type: Number, required: true },
});

//Created name schema as sub document for the schema of User
let fullname = S({
  // firstName is a simple String type that is required
  // lastName is a simple String type that is required
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
});


//Created name schema as sub document for the schema of User
let SchemaOfRecipe = S({
  // name is a simple String type that is required
   name: { type: String, required: true },
  // description is a simple String type that is not required
   description: { type: String, required: false },
  // pictureURL is a simple String type that is required
   pictureURL: { type: String, required: true },
  // pictureURL is a simple Number type that is required
  prepTime: { type: Number, required: true },
  // cookingTime is a Number String type that is required
  cookingTime: { type: Number, required: true },
  // directions is a an array of String types that is required
  directions: { type: [String], required: true },
  // ingredients is a list of of sub document Sub Ingredient types.
  ingredients: {
       list: { type: [SubIngredient], required: true }
   },
  // review is an array of schema Object type
   review: { type: [S.ObjectId], ref: 'Review' }
});


 // this schema represents the reviews
let SchemaOfReview = S({
    // recipe is a schema object type
   recipe: { type: S.Types.ObjectId, ref: 'Recipe' },
    // description is a string type that is required.
   description: { type: String, required: true },
  // rating is a number type which only accepts number min : 1 and max : 5, and is required.
   rating: { type: Number, min: 1, max: 5, required: [true, 'RatingsNotGiven'] },
  // date is a Date type default to the current date
   date: { type: Date, default: Date.now },
  // user is a schema object type
  user: { type: S.Types.ObjectId, ref: 'User' }
});



// This schema represents the name of the user
let SchemaOfUser = S({
  /*
   fullname is a subdocument of User, and will be stored
   in the same document as the User itself.
   username and email is simply a String type that is unique and required.
  */
   name: fullname,
   username: { type: String, unique: true, required: true },
   email: { type: String, unique: true, required: true },
});



/*
  This section creates interactive models from the defined schemas
  above so that you can perform Create Read Update and Delete (CRUD)
  operations against the schemas.
  NOTE since the fullname is embedded within SchemaOfUser it does NOT have
  to be created as a model!
 */

let Recipe = mongoose.model('Recipe', SchemaOfRecipe);
let Review = mongoose.model('Review', SchemaOfReview);
let User = mongoose.model('User', SchemaOfUser);

// Export the three created models
export { Recipe, Review, User };
